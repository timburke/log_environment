#utilities.py

import os
import signal
import datetime
import threading

exit_event = threading.Event()

def abort():
    """
    Send ourselves a SIGINT.
    """

    exit_event.set()


def format_time(timestamp):
    """
    Format a timestamp as a string
    """

    date = datetime.datetime.fromtimestamp(timestamp)

    return "%04d/%02d/%02d %02d:%02d:%02d" % (date.year, date.month, date.day, date.hour, date.minute, date.second)
