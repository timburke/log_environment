#######################################################################################
# log_enivonment - a python module for logging data from a USB-6009 NI DAQ instrument #
# periodically.                                                                       #
#######################################################################################

##############################################
#Program Code - Do not modify below this line#
##############################################

import os.path
import time
import acquire
import datasaver
import logger
import utilities
import tasks
from config import *
import signal
import sys
from collections import deque
import temp


def cleanup(msg):
    #Tell the acquisition threads to stop
    acquire_thread.stop()
    temp_read_thread.stop()
    
    acquire_thread.join()
    temp_read_thread.join()

    #Tell the data saver thread to stop and fire the save_event to make sure it wakes up
    daqsaver_thread.stop()
    acquire_thread.save_event.set()
    daqsaver_thread.join()

    tempsaver_thread.stop()
    temp_read_thread.save_event.set()
    tempsaver_thread.join()

    #Tell logger thread to stop and then send it a message to make sure it wakes up
    logger_thread.stop()
    log.info(msg)
    logger_thread.join()

    task_thread.stop()
    task_thread.queue.put(lambda: 0)
    task_thread.join()


#Signal handler
def interrupt_handler(signal, frame):
    """
    Handle interrupt signal by cleaning up and then quitting.
    """
    print "In interrupt handler"
    cleanup("Shutting down environmental sampling program because of SIGINT")
    sys.stdout.flush()
	
    sys.exit(0)


def temp_formatter(entries):
    """
    Format a tuple of 3-tuples containing (name, time, temp) each as a separate
    csv line
    """

    line = ""

    for (name, time, temp) in entries:
        line += "%s, %s, %.3f\n" % (name, utilities.format_time(time), temp)

    return line

#Setup our signal handler to kill the process on ^C
signal.signal(signal.SIGINT, interrupt_handler)

errlog = os.path.join(maindir, errfile)

#Set up a thread for asynchronous tasks that cannot fail
task_thread = tasks.TaskThread()
task_queue = task_thread.queue
task_thread.start()

#Setup the logger and start it
logger_thread = logger.LoggerThread(errlog, tasks=task_queue)
log = logger_thread.queue
logger_thread.start()

log.info("Beginning Environmental Sampling Program")

#Setup the sampling thread
sample_queue = deque(maxlen=2*save_interval)  # #Make sure the buffer can't grow out of control
temp_queue = deque(maxlen=2*len(temperature_sensors)*save_interval)

acquire_thread = acquire.SampleAcquisitionThread(sample_rate=daq_sample_rate,
                                                 average_window=average_window,
                                                 save_interval=save_interval,
                                                 output_queue=sample_queue,
                                                 logger=log,
                                                 tasks=task_queue)
acquire_thread.start()

temp_read_thread = temp.TemperatureAcquisitionThread(sample_rate=temp_delay,
                                                     save_interval=save_interval,
                                                     output_queue=temp_queue,
                                                     logger=log,
                                                     tasks=task_queue)
temp_read_thread.start()

#Set up the thread to actually save DAQ data out to csv
#oxygen sensor is accurate to .1 ppm
#water sensor is accurate to 2C
daq_formatter = lambda entry: "%s,%.1f,%.0f,%.0f\n" % (utilities.format_time(entry[0]), entry[1], entry[2], entry[3])
daqsaver_thread = datasaver.DataSaverThread(os.path.join(maindir, subdir), envfile_prefix + " ",
                                            acquire_thread.save_event, sample_queue, task_queue, log, formatter=daq_formatter,
                                            timeout=daq_timeout)
daqsaver_thread.start()

#Set up the thread to save temperature data out to csv
tempsaver_thread = datasaver.DataSaverThread(os.path.join(maindir, subdir), tempfile_prefix + " ",
                                             temp_read_thread.save_event, temp_queue, task_queue, log, formatter=temp_formatter,
                                             timeout=temp_timeout)
tempsaver_thread.start()

#Wait forever unless someone sends us an interrupt signal.
#Windows can't pause so do a loop
while True:
	time.sleep(1.0)
	if utilities.exit_event.is_set():
		break

print "Exit Event Signalled"
cleanup("Exited logging program due to an interrupt or an exception")
