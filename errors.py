#errors.py
#Nice error strings to send as emails

io_error = "There was a file system related error writing to a necessary file"
timeout_error = "The thread responsible for saving data from the oxygen and moisture sensors was not woken up with more data in too long. The is most likely a problem with the DAQ board."
logging_io_error = "The thread responsible for logging informational messages about program status had an IO error and was stopped.  No more messages will be logged but the rest of the logger will continue running."
temp_error = "There was an error communicating with the temperature sensors"
task_error = "The task that was executed produced an exception.  This is not fatal."