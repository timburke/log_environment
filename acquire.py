#aquire.py
#A thread that continually logs data from the NI DAQ board to a circular buffer

import threading
import time
from stoppable import StoppableThread
import numpy as np
import mail
import traceback
from PyDAQmx import Task, DAQError
from PyDAQmx.DAQmxConstants import *
from PyDAQmx.DAQmxTypes import *

class SampleAcquisitionThread (StoppableThread):
    def __init__(self, sample_rate, average_window, save_interval, output_queue, logger, tasks):
        """
        Sample continuously the DAQ inputs and read them into a buffer that is average_window
        in size.  Whenever the buffer is filled, average the results and store that into a
        common queue.  Trigger save_event whenever the queue has reached a certain size set
        by save_freq.
        """

        self.sample_rate = sample_rate
        self.average_window = average_window
        self.save_interval = save_interval
        self.save_event = threading.Event()
        self.output = output_queue
        self.logger = logger
        self.tasks = tasks

        self.buffer = np.zeros([self.average_window*2], dtype=np.float64) #twice as big because we have two channels
        self.range_buffer = np.zeros(1,dtype=np.uint8)
		
        super(SampleAcquisitionThread, self).__init__()
    
    def _setup_daq(self):
        """
        Setup a task on the daq board Dev1 that samples differential inputs ai0 and ai1 at the given sampling rate
        continuously.
        """

        self.input_task = Task()
        self.range_task = Task()
        self.num_read = int32()
        self.num_ranges_read = int32()

        #Create a channel reading differential inputs 0 and 1
        try:
            self.input_task.CreateAIVoltageChan("Dev1/ai0:1", "", DAQmx_Val_Diff, 0.0, 2.0, DAQmx_Val_Volts, None)
            self.range_task.CreateDIChan("Dev1/port0/line0:2", "", DAQmx_Val_ChanForAllLines)
        except DAQError as e:
            self.logger.error("Error creating voltage channel. Stopping logging service.")
            print e
            raise

        try: 
            self.input_task.CfgSampClkTiming("", self.sample_rate, DAQmx_Val_Rising, DAQmx_Val_ContSamps, self.average_window*100)
        except DAQError as e:
            self.logger.error("Error creating clock timer.  Stopping logging service.")
            print e
            raise
    def _determine_range(self):
        """
        Figure out what range we're in from the TTL lines
        """
        
        val = self.range_buffer[0]
        
        if val == 1:
            return 10.0
        elif val == 2:
            return 100.0
        elif val == 4:
            return 1000.0
        
        raise exception("Invalid Range value from TTL Signal, expect (1,2 or 4) got %d" % val)
    
    def _average_and_save(self):
        #Do the conversion and averaging over the sample window

        range = self._determine_range()
		
        ai0_unconv = np.mean(self.buffer[0:self.average_window])
        ai1_unconv = np.mean(self.buffer[self.average_window:])
        ai0_mean = np.mean(self.buffer[0:self.average_window])            #Oxygen Sensor
        ai1_mean = np.mean(self.buffer[self.average_window:]*15.957 - 130.0)    #Water Sensor
        
        ai0_mean = ai0_mean / 2.0 * range  			#Reading is linear 0-2V full scale with autoranging
		
        print "Oxygen (%f V, %f converted)" % (ai0_unconv, ai0_mean)
        print "Range: %s, inferred: %f" % (bin(self.range_buffer[0]), range)
        #print "Dew Point (%f V, %f converted)" % (ai1_unconv, ai1_mean)

        #Output our timestamped values to the queue to be written to the log file
        self.output.append((time.time(), ai0_mean, ai1_mean, range))

    def fake_data(self):
        """
        Generate fake sensor data at the appropriate rate for testing purposes.
        """

        time.sleep((self.average_window/self.sample_rate))
        self.buffer[:] = 1.0
        print "Faking data"

    def run(self):
        try:
            self._setup_daq()
        except DAQError:
            msg = traceback.format_exc()
            self.tasks.put(lambda: mail.send_error(msg))
            return

        #Start sampling
        try:
            self.input_task.StartTask()
        except DAQError:
            msg = traceback.format_exc()
            self.tasks.put(lambda: mail.send_error(msg))
            self.logger.error("Could not start sampling task, aborting.")
            return

        self.logger.info("Sampling has begun")

        sample_count = 0

        #Loop forever reading in a window 
        while True:
            try:
                self.input_task.ReadAnalogF64(self.average_window, -1.0, DAQmx_Val_GroupByChannel, self.buffer, self.buffer.shape[0], byref(self.num_read), None)
                self.range_task.ReadDigitalU8(1, -1.0, DAQmx_Val_GroupByChannel, self.range_buffer, self.buffer.shape[0], byref(self.num_ranges_read), None)
            except DAQError:
                msg = traceback.format_exc()
                self.tasks.put(lambda: mail.send_error(msg))
                self.logger.error("Could not read samples from DAQ board, aborting logging service.")
                break 
                
            #Check if we should stop and clean up
            if self.stopped():
                break
                
            #Average and output the mean
            self._average_and_save()
            
            #Check if we should alert the saving thread
            sample_count += 1
            if sample_count == self.save_interval:
                #Wake up the save thread to process these samples
                sample_count = 0
                
                #If the other thread has still not been called since the last data was sent
                #there must be an error, so report it and break
                if self.save_event.is_set():
                    self.logger.error("Data saving thread is not processing data fast enough, aborting logging system")
                    self.tasks.put(lambda: mail.send_error('Data saving thread is not processing data fast enough, try increasing the interval between saving samples or reduce system load.'))
                    break   
                
                self.save_event.set()

        #Clean up all of our shared resources
        try:
            self.input_task.ClearTask()
        except DAQError:
            msg = traceback.format_exc()
            self.tasks.put(lambda: mail.send_error(msg))
            self.logger.error("Could not start sampling task, aborting.")
            return 
