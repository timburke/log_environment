#datasaverthread.py
#A thread that waits on an event and then processes all the data samples in the queue
#saving them to a file

import datetime
import os.path
import stoppable
import config
import mail
import utilities
import traceback
import errors


class DataSaverThread (stoppable.StoppableThread):
    """
    A thread that opens a logging file and appends messages to it as they are added to a
    loggging queue.
    """

    def __init__(self, data_dir, name_prefix, event, queue, tasks, logger, formatter, timeout):
        self.event = event
        self.dir = data_dir
        self.prefix = name_prefix
        self.queue = queue
        self.tasks = tasks
        self.logger = logger
        self.formatter = formatter
        self.timeout = timeout

        self.file = open(self._build_filename(), "a")

        super(DataSaverThread, self).__init__()

    def _build_filename(self):
        path = os.path.join(self.dir, self.prefix)

        today = datetime.date.today()
        self.last_day = today

        date_str = "(%02d-%02d-%04d)" % (today.month, today.day, today.year)

        path += date_str + ".csv"

        return path

    def _check_new_day(self):
        """
        Check if a new day has happened since the last time we saved data so that we
        can create a new file.
        """

        today = datetime.date.today()

        #If we just passed midnight, close the old file and open a new one
        if today != self.last_day:
            self.file.close()
            self.file = open(self._build_filename(), "a")

    def _format_entry(self, entry):
        """
        Format the 3-tuple from the daq thread and write it in csv format
        """

        line = self.formatter(entry)
        return line

    def run(self):
        while True:
            signalled = self.event.wait(self.timeout)

            #Check if we timed out
            if not signalled:
                #We should have gotten more data by now, so report the error and shut down
                self.logger.error('Data saving thread timed out. Quitting.')
                self.tasks.put(lambda: mail.send_error(errors.timeout_error))
                utilities.abort()
                break

            #We're good, clear the flag and process the samples
            self.event.clear()

            #write all the queued entries to our file
            try:
                while True:
                    entry = self.queue.popleft()

                    self._check_new_day()  # #Check if we need to roll over our log file
                    line = self._format_entry(entry)
                    self.file.write(line)
            except IndexError:
                self.file.flush()  # #Error just means we're done with the entries
            except IOError:
                #If there was any error writing to the file, we email the error and abort
                self.tasks.put(lambda: mail.fatal_exception(errors.io_error, traceback.format_exc()))
                utilities.abort()
                break

            #Check if we should stop
            if self.stopped():
                break

        self.file.close()
