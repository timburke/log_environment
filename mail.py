#email.py
#Utility function for sending an email using the mcgehee.notify@gmail.com email address

import smtplib
import traceback
import config
from email.MIMEText import MIMEText

username = 'mcgehee.notify@gmail.com'
password = '^la$G225yD'
server = 'smtp.gmail.com'
port = 587

def send_mail(to, subject, contents):
	"""
	Send an email using the mcgehee.notify gmail address.
	"""
	
	msg = MIMEText(contents)
	msg['To'] = to
	msg['From'] = username
	msg['Subject'] = subject
	
	session = smtplib.SMTP(server, port)
	session.ehlo()
	session.starttls()
	session.login(username, password)
	session.sendmail(username, to, msg.as_string())
	session.quit()

def warn_exception(context, msg, to=None):
	sev = "A NONfatal excpeption occurred in the logging program. It should however be investigated because it is indicative of larger problems"

	send_exception(sev, context, msg, to)
	
def fatal_exception(context, msg, to=None):
	sev = "A fatal exception occurred in the logging program.  The logging program has been shut down due to this exception and needs to be restarted."

	send_exception(sev, context, msg, to)

def send_exception(severity, context, msg, to=None):
	"""
	Send the contents of the exception to the address given.  
	msg should be the result of a call to traceback.format_exc()
	"""
	
	if to is None:
		to = config.administrator_email
		
	contents = severity + "\n"
	contents += "The context for the error was:\n" + context + "\n"
	contents += "The actual excepton was:\n"
	contents += msg
	
	send_mail(to, 'A fatal error occured on the environmental logging system.', contents)

def send_error(msg):
	to = config.administrator_email
		
	send_mail(to, 'A fatal error occured on the environmental logging system.', msg)