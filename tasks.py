#tasks.py
#A thread for running background tasks.

import threading, time, datetime
import stoppable
import Queue
import mail
import traceback
import errors

class TaskThread (stoppable.StoppableThread):
	"""
	A thread that opens a logging file and appends messages to it as they are added to a 
	loggging queue.
	"""
	def __init__(self):
		self.queue = Queue.Queue()
		
		super(TaskThread, self).__init__()
	
	def run(self):
		"""
		Run forever. waiting for log entries and then writing them to the open file as they 
		come in.
		"""
		
		while True:
			task = self.queue.get()
			
			#Task must be a callable object, eat all exceptions and report them since this
			#thread must never fail.
			try:
				task()
			except:
				mail.warn_exception(errors.task_error, traceback.format_exc())
			
			#Check if we should shut down
			if self.queue.empty() and self.stopped():
				break