#logger.py
#A thread that checks for new entries in a logging queue and logs them to a file

import threading, time, datetime
import stoppable
import Queue
import traceback
import errors

class LoggerQueue:
	"""
	A threadsafe queue that can take info, error and warning messages and format them
	"""
	
	def __init__(self):
		self.queue = Queue.Queue()
	
	def _format(self, entry):
		line = "(%s) %s: %s\n" % (self._format_time(entry[2]), entry[0], entry[1])
		
		return line
	
	def _format_time(self, timestamp):
		date = datetime.datetime.fromtimestamp(timestamp)
		
		return "%02d/%02d/%04d %02d:%02d:%02d" % (date.month, date.day, date.year, date.hour, date.minute, date.second)
	
	def error(self, msg):
		self.queue.put(("ERROR", msg, time.time()))

	def info(self, msg):
		self.queue.put(("INFO", msg, time.time()))
	
	def warn(self, msg):
		self.queue.put(("WARNING", msg, time.time()))

	def pop(self):
		"""
		Pop off the oldest entry, format it and return it.  Will block if there are no
		messages in the queue.
		"""
		
		entry = self.queue.get()
		
		return self._format(entry)


class LoggerThread (stoppable.StoppableThread):
	"""
	A thread that opens a logging file and appends messages to it as they are added to a 
	loggging queue.
	"""
	def __init__(self, filepath, tasks):
		self.queue = LoggerQueue()
		
		self.file = open(filepath, "a")
		self.tasks = tasks
		
		super(LoggerThread, self).__init__()
	
	def run(self):
		"""
		Run forever. waiting for log entries and then writing them to the open file as they 
		come in.
		"""
		
		while True:
			entry = self.queue.pop()
			
			#Write the entry to the file and make sure its committed to disk
			try:
				self.file.write(entry)
				self.file.flush()
			except IOError:
				self.tasks.put(lambda: mail.warn_exception(errors.logging_io_error, traceback.format_exc()))
				break
			
			#Check if we should shut down
			if self.stopped():
				break
		
		#Clean up everything
		self.file.close()
		