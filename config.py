#########################
#Configurable Parameters#
#########################

#Who should receive emails notifying them about errors in this logging system?
#By default, all exceptions are fatal and cause an email to be sent to this address
#with details.
administrator_email = 'timburke@stanford.edu'

#The base directory under which the status log and data files will live
maindir = "C:\Documents and Settings\Owner\My Documents\Logs"

#The subdirectory under maindir where the data files will be stored
subdir = "Environment"

#The name of the status log that logs informational and error messages
errfile = "environmental_sampling.log"

#The prefix for the name of the files that DAQ data will be logged to
#the files will be created in the form "prefix (yyyy/mm/dd).csv"
envfile_prefix = "environment"

#The prefix for the name of the files that temperature data will be logged to
#the files will be created in the form "prefix (yyyy/mm/dd).csv"
tempfile_prefix = "temp"

#The sample rate in hertz.  This is the rate at which data is continuously gathered
#from the sensors
daq_sample_rate = 1.0

#This is the time that the temperature acquisition loop sleeps between subsequent attempts
#to gather data from each temperature sensor.  Reading a temperature sensor takes 1 second
#per sensor.  So, i.e. reading 2 sensors with a delay of 8 seconds would give you data once
#every 10 seconds (8+1+1)
temp_delay = 2.0

#Every data point reported by the program will be the average of the last X samples.
#This average_window variable sets how many samples you average over.  So the time between
#samples would be 1/(sample_rate)*average_window seconds.
average_window = 5

#How often should samples be dumped to the data file.  This does not affect the values
#written or how often samples are gathered.  It just says write them in blocks of X for
#efficiency rather than writing each one individually.  So the file will be updated every
#1/(sample_rate)*average_window*save_interval seconds
save_interval = 10


#Data saving thread timeout
#If there is no new data to be saved in this window (by default twice as long as it should take)
#the acquisition thread will be assumed to be broken and an email will be sent with the error so
#that an administrator can fix it.
daq_timeout = 2.0/(daq_sample_rate)*average_window*save_interval

#The serial number of all sensors that we should log temperature from
temperature_sensors = [("2822afcd01000095", "Sensor 1")]
must_log_temperature = True
temp_timeout = save_interval*(len(temperature_sensors)+temp_delay)*2