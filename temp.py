#temp.py
#Acquire temperature readings from the 1-wire DS18B20 temperature readers and log them

import threading
import time
from stoppable import StoppableThread
import mail
import traceback
from pywire import tmex
import utilities
import config
import errors


class TemperatureAcquisitionThread (StoppableThread):
    def __init__(self, sample_rate, save_interval, output_queue, logger, tasks):
        """
        Sample the temperature sensors given and
        """

        self.sample_rate = sample_rate
        self.save_interval = save_interval
        self.save_event = threading.Event()
        self.output = output_queue
        self.logger = logger
        self.tasks = tasks
        self.session = None

        super(TemperatureAcquisitionThread, self).__init__()

    def _create_dev_map(self):
        devs = self.session.enumerate()

        self.devs = {}

        for dev in devs:
            self.devs[dev.info['serial']] = dev

    def _setup(self):
        sensors = config.temperature_sensors
        required = config.must_log_temperature
        
        mex = tmex.TMEX()
                
        if len(sensors) == 0 and required:
            self.logger.error("Temperature logging is required and no temperature sensors were configured.")
            utilities.abort()

            return False

        self.session = mex.default_session()

        self._create_dev_map()

        self.sensors = {}

        #Find all the sensors that we're configured to use
        for serial, name in sensors:
            if serial in self.devs:
                print "Adding tempearature sensor %s" % name
                self.sensors[name] = self.devs[serial]
            else:
                self.logger.error("A configured temperature sensor was not found (serial: %s, name: %s).  Ignoring that sensor." % (serial, name))

                if required:
                    self.tasks.put(lambda: mail.send_error("Temperature sensing is listed as required and could not be started.  Read the log for more details."))
                    utilities.abort()

                    return False
        return True

    def run(self):
        sample_count = 0

        try:
            if self._setup():
                while True:
                    time.sleep(self.sample_rate) #sample rate is the delay between subsequent acquisitions c.f. config.py temp_delay

                    readings = []
                    #query each sensor
                    for name, dev in self.sensors.iteritems():
                        temp = dev.get_temperature()
                        print "Sensor %s temp: %f" % (name, temp)
                        readings.append((name, time.time(), dev.get_temperature()))

                    #check if we should stop
                    if self.stopped():
                        break

                    self.output.append(tuple(readings))
                    sample_count += 1

                    #check if we need to save the data
                    if sample_count == self.save_interval:
                        #Wake up the save thread to process these samples
                        sample_count = 0

                        #If the other thread has still not been called since the last data was sent
                        #there must be an error, so report it and abort
                        if self.save_event.is_set():
                            self.logger.error("Data saving thread is not processing data fast enough, aborting logging system")
                            self.tasks.put(lambda: mail.send_error('Data saving thread for temperature is not processing data fast enough, try increasing the interval between saving samples or reduce system load.'))
                            utilities.abort()
                            break

                        self.save_event.set()
        except (ValueError, EnvironmentError) as e:
            print e
            self.tasks.put(lambda: mail.fatal_exception(errors.temp_error, traceback.format_exc()))
            utilities.abort()
        except:
            self.logger.error("Unknown eception\n%s" % traceback.format_exc())
            self.tasks.put(lambda: mail.fatal_exception(errors.temp_error, traceback.format_exc()))
            utilities.abort()
        finally:
            if self.session is not None:
                self.session.close()
